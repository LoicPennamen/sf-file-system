# SymfonyFileSystem #

This package aims to provide a quickstart for file management in Symfony.


### Installation ###

* Add to your project using composer:
    ```
    composer require loicpennamen/sf-file-system
    ```

* Add routes in `/config/routes.yaml`:
    ```
    filesystem:
      resource: '../vendor/loicpennamen/sf-file-system/src/Controller/'
      type: annotation
    ```


### TO-DOs ###

* Write readme instructions
* Symfony Flex recipe : https://github.com/symfony/recipes
* Interface with sf-front-editor


### Who do I talk to? ###

Loïc Pennamen  
http://loicpennamen.com
