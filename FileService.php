<?php

namespace LoicPennamen\FileSystemBundle;

use LoicPennamen\FileSystemBundle\Entity\File;
use LoicPennamen\StringService\StringService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class FileService
{
	private $router;
	private $tl;
	private $stringService;
	private $em;
	private $projectDir;
	private $twig;
	public $allowedExtensions = [
		'jpg','jpeg','png','gif','bmp',
		'mpeg','mpg','mp3','mp4','mp5','avi','mov',
		'pdf','doc','docx','xls','csv','ods',
		'xml',
	];
	private $imageVersions = [
		// bigger to smaller , for fallbacks
		'lg' => ['w' => 2000,	'h' => 2000, 	'crop' => false],
		'md' => ['w' => 1000, 	'h' => 1000, 	'crop' => false],
		'sm' => ['w' => 600, 	'h' => 600, 	'crop' => false],
		'th' => ['w' => 80, 	'h' => 80, 		'crop' => false],
	];

	public function __construct(RouterInterface $router, TranslatorInterface $tl, EntityManagerInterface $em, StringService $stringService, $projectDir, Environment $twig)
	{
		$this->router = $router;
		$this->tl = $tl;
		$this->stringService = $stringService;
		$this->projectDir = $projectDir;
		$this->em = $em;
		$this->twig = $twig;
	}

	/**
	 * @param File $file
	 * @param User|null $user
	 */
	public function markAsRead(File $file, User $user = null)
	{
		if(!$user)
			return;

		$file->addReadBy($user);
		$this->em->persist($file);
		$this->em->flush();
	}

	/**
	 * @param Agency $agency
	 * @return File[]
	 */
	public function getUnreadAgencyFiles(Agency $agency, User $user)
	{
		/** @var QueryBuilder $qb */
		$qb = $this->em->getRepository(File::class)->createQueryBuilder('file');
		return $qb
			->where(':user NOT MEMBER OF file.readBy')->setParameter('user', $user)
			->andWhere('file.context LIKE :context')->setParameter('context', 'agencies-share')
			->andWhere('file.isAvailableToAllAgencies = TRUE OR :agency MEMBER OF file.agenciesWhitelist')->setParameter('agency', $agency)
			->orderBy('file.title')
			->getQuery()
			->getResult()
		;
	}

	/**
	 * @param Agency $agency
	 * @return File[]
	 */
	public function getAgencyFiles(Agency $agency)
	{
		/** @var QueryBuilder $qb */
		$qb = $this->em->getRepository(File::class)->createQueryBuilder('file');
		return $qb->where('file.context LIKE :context')->setParameter('context', 'agencies-share')
			->andWhere('file.isAvailableToAllAgencies = TRUE OR :agency MEMBER OF file.agenciesWhitelist')->setParameter('agency', $agency)
			->orderBy('file.title')
			->getQuery()
			->getResult()
		;
	}

	/**
	 * @param File $file
	 * @param bool $flush
	 * @return File
	 */
	public function duplicate(File $file, $flush = true)
	{
		$dir = $this->getUploadedFilesDir() . '/';
		$pathParts = pathinfo($this->getFullFilePath($file));
		$ext = $pathParts['extension'];
		$oldFilename = $file->getFileName();
		$newFilename = uniqid().'.'.$ext;

		// Copie du fichier
		if(is_file($dir.$oldFilename))
			copy($dir.$oldFilename, $dir.$newFilename);

		// Copie des versions
		foreach($this->imageVersions as $versionSlug => $config) {
			$oldVersionFileName = $this->stringService->suffixFileName($oldFilename, '-' . $versionSlug);
			$newVersionFileName = $this->stringService->suffixFileName($newFilename, '-' . $versionSlug);
			if(is_file($dir.$oldVersionFileName))
				copy($dir.$oldVersionFileName, $dir.$newVersionFileName);
		}

		// METAS
		$newFile = new File();
		$newFile->setContext($file->getContext());
		$newFile->setDescription($file->getDescription());
		$newFile->setFileName($newFilename);
		$newFile->setFilesize($file->getFilesize());
		$newFile->setFiletype($file->getFiletype());
		$newFile->setOriginalFileName($file->getOriginalFileName());
		$newFile->setTitle($file->getTitle());
		$newFile->setWinUp($file->getWinUp());

		$this->em->persist($newFile);
		if($flush)
			$this->em->flush();

		return $newFile;
	}

	/**
	 * Petites images unqiuement !
	 * @param $filePath
	 * @return string
	 */
	public function base64Img($filename)
	{
		// Absolute path
		if($this->projectDir !== substr($filename, 0, strlen($this->projectDir)))
			$filePath = $this->projectDir . '/' . $filename;
		else
			$filePath = $filename;

		if(!is_file($filePath))
			return '';

		$ext = pathinfo($filePath, PATHINFO_EXTENSION);
		$imgbinary = fread(fopen($filePath, "r"), filesize($filePath));
		return 'data:image/'.$ext.';base64,'.base64_encode($imgbinary);
	}

	/**
	 * @param File $file
	 * @return float|int
	 */
	public function getImageRatio(File $file)
	{
		if(true !== $this->isImage($file))
			return -1;

		$size = getimagesize($this->getFullFilePath($file));
		if(!is_array($size))
			return -1;

		return $size[0] / $size[1];
	}

	/**
	 * @param File $file
	 * @return bool
	 */
	public function isImage(File $file)
	{
		return in_array($file->getFiletype(), ['jpg','jpeg','png','gif']);
	}

	/**
	 * @param array $options
	 * @return int
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function count(array $options = [])
	{
		/** @var QueryBuilder $qb */
		$qb = $this->em->getRepository(File::class)->createQueryBuilder('file');
		$qb->select('COUNT(file)');

		foreach (['context','participation','winUp'] as $key){
			if(key_exists($key, $options))
				$qb->andWhere('file.'.$key.' = :'.$key)->setParameter($key, $options[$key]);
		}

		$result = $qb->getQuery()->getOneOrNullResult();
		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	/**
	 * @param Request $request
	 * @return array
	 * @throws \Exception
	 */
	public function handleUpload(Request $request)
	{
		$newFiles = [];

		// One by one
		for ($i = 0; $i < sizeof($request->files->all()['files']); $i++) {
			$uploadedFile = $request->files->all()['files'][$i];

			if(!$uploadedFile instanceof UploadedFile)
				throw new \Exception("Uploaded file $i not found");

			$file = $this->handleUploadedFile($uploadedFile, $request, $i);
			$newFiles[] = $file;

			// Preview
			$previewHtml = $this->twig->render('file/preview-remove.html.twig', ['file' => $file]);
		}

		// Legacy : Le preview ne retourne qu'un seul résultat. Peut être amélioré avec refactoring
		return [
			'success' => true,
			'url' => $this->getUrl($file),
			'file' => $file,
			'previewHtml' => $previewHtml,
			'newFiles' => $newFiles,
		];
	}

	/**
	 * @param UploadedFile $uploadedFile
	 * @param Request $request
	 * @return File
	 */
	public function handleUploadedFile(UploadedFile $uploadedFile, Request $request = null, $i = null, $flush = true)
	{
		$request = $request ?: new Request();
		$originalFilename = $uploadedFile->getClientOriginalName();
		$ext = mb_strtolower($uploadedFile->getClientOriginalExtension());
		$guessedExt = $uploadedFile->guessExtension();
		$newFilename = uniqid().'.'.$ext;
		$size = $uploadedFile->getSize();
		$context = $request->get('context') ?: 'upload';
		$winup = $request->get('winup')
			? $this->em->getRepository(WinUp::class)->find($request->get('winup'))
			: null
		;
		$title = is_array($request->request->get('titles')) && count($request->request->get('titles')) > $i
			? $request->request->get('titles')[$i]
			: $originalFilename
		;
		$confidentiality = is_array($request->request->get('confidentialities')) && count($request->request->get('confidentialities')) > $i
			? $request->request->get('confidentialities')[$i]
			: null
		;

		// Ext Checks
		if(!in_array($ext, $this->allowedExtensions))
			throw new AccessDeniedException("Extension $ext is not allowed : $originalFilename");
		if(!in_array($guessedExt, $this->allowedExtensions))
			throw new AccessDeniedException("Guessed extension $guessedExt is not allowed : $originalFilename");

		// By context
		switch($context){
			case 'update-user':
			case 'user-avatar':
				$avatarExt = ['jpg','jpeg','png'];
				if(!in_array($ext, $avatarExt))
					throw new AccessDeniedException("Les formats de fichier autorisés sont : ".implode(', ', $avatarExt));
				break;
		}
		
		// Do save
		$uploadedFile->move(
			$this->getUploadedFilesDir(),
			$newFilename
		);

		// New file entity
		$file = new File();
		$file->setContext($context);
		$file->setOriginalFileName($originalFilename);
		$file->setFileName($newFilename);
		$file->setFiletype($guessedExt);
		$file->setFilesize($size);
		$file->setTitle($title);
		$file->setConfidentiality($confidentiality);
		$file->setWinUp($winup);

		$this->em->persist($file);
		if($flush)
			$this->em->flush();

		// Callbacks
		$this->makeMetaFiles($file);

		return $file;
	}

	/**
	 * Génère les images et formate les fichiers requis
	 * @param File $file
	 */
	private function makeMetaFiles(File $file)
	{
		// Images jpg
		if(true === $this->isImage($file))
			$this->makeImageVersions($file);
	}

	/**
	 * @param File $file
	 */
	private function makeImageVersions(File $file)
	{
		$dir = $this->getUploadedFilesDir();
		$filePath = $this->getFullFilePath($file);
		list($width, $height, $type, $attr) = getimagesize($filePath);
		$landscape = $width/$height > 1;

		// For each version
		foreach($this->imageVersions as $versionSlug => $config){
			$tmpFileName = $this->stringService->suffixFileName(
				$file->getFileName(),
				'-'.$versionSlug
			);

			// Simple copy if smaller than target
			if( ($landscape && $width <= $config['w']) || (!$landscape && $height <= $config['h']) ){
				copy($filePath , $dir.'/'.$tmpFileName);
				continue;
			}

			$image = new \Imagick($filePath);
			$image->setImageCompression(\Imagick::COMPRESSION_JPEG);
			$image->setImageCompressionQuality(70);
			$image->thumbnailImage(
				$landscape ? $config['w'] : 0,
				$landscape ? 0 : $config['h']
			);

			$image->writeImage($dir . '/' . $tmpFileName);
		}
	}

	/**
	 * @param File $file
	 * @return string
	 */
	public function getUrl(File $file, $version = 'md')
	{
		return $this->router->generate('app_file_get',
			[
				'fileName'=>$file->getFileName(),
				'version' => $version,
			],
			UrlGeneratorInterface::ABSOLUTE_URL
		);
	}

	/**
	 * @param File $file
	 * @return string
	 */
	public function getFullFilePath(File $file, $version = null)
	{
		$dir = $this->getUploadedFilesDir() . '/';
		$fileName = $file->getFileName();

		if($version){
			$tmpFileName = $this->stringService->suffixFileName($fileName, '-'.$version);
			if(file_exists($dir.$tmpFileName))
				$fileName = $tmpFileName;
		}

		return $dir.$fileName;
	}

	/**
	 * @return string
	 */
	public function getUploadedFilesDir()
	{
		$dir = $this->projectDir . '/uploads';
		if(!file_exists($dir))
			mkdir($dir);
		return $dir;
	}

	/**
	 * @return string
	 */
	public function getUploadAcceptStr()
	{
		return '.'.implode(',.', $this->allowedExtensions);
	}

	/**
	 * @return false|float|int
	 * @see https://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
	 */
	public function getUploadMaxSize()
	{
		static $max_size = -1;

		if ($max_size < 0) {
			// Start with post_max_size.
			$post_max_size = $this->parseSize(ini_get('post_max_size'));
			if ($post_max_size > 0) {
				$max_size = $post_max_size;
			}

			// If upload_max_size is less, then reduce. Except if upload_max_size is
			// zero, which indicates no limit.
			$upload_max = $this->parseSize(ini_get('upload_max_filesize'));
			if ($upload_max > 0 && $upload_max < $max_size) {
				$max_size = $upload_max;
			}
		}
		return $max_size;
	}

	/**
	 * @param $size
	 * @return false|float
	 * @see https://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
	 */
	private function parseSize($size) {
		$unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
		$size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
		if ($unit) {
			// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
			return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
		}
		else {
			return round($size);
		}
	}

	/**
	 * @param File $file
	 */
	public function delete(File $file, $flush = true)
	{
		$dir = $this->getUploadedFilesDir();
		$removeList = [
			$dir . '/' . $file->getFileName()
		];

		// + images versions
		foreach ($this->imageVersions as $versionSlug => $config) {
			$removeList[] = $dir . '/' . $this->stringService->suffixFileName(
				$file->getFileName(),
				'-' . $versionSlug
			);
		}

		// Hard delete
		foreach($removeList as $tmpFileName){
			if(file_exists($tmpFileName))
				unlink($tmpFileName);
		}

		// Db remove
		$this->em->remove($file);
		if($flush)
			$this->em->flush();
	}

	/**
	 * @param $id
	 * @return File|object|null
	 */
	public function findById($id)
	{
		if(!$id)
			return null;
		return $this->em->getRepository(File::class)->find($id);
	}

	/**
	 * @param array $options
	 * @return int|mixed|string
	 */
	public function find(array $options)
	{
		$config = array_merge([
			'extensions' => null,
			'search' => null,
			'max' => 10,
			'offset' => 0,
			'orderBy' => 'file.creationDate',
			'direction' => 'DESC',
			'user' => null,
			'context' => null
		], $options);

		/** @var QueryBuilder $qb */
		$qb = $this->em->getRepository(File::class)->createQueryBuilder('file');
		$qb->addOrderBy($config['orderBy'], $config['direction']);
		$qb->setMaxResults($config['max']);
		$qb->setFirstResult($config['max'] * $config['offset']);

		// Filtrer par ext.
		if($config['extensions'])
			$qb->andWhere('file.filetype IN(:extensions)')->setParameter('extensions', $config['extensions']);

		// Filtrer par contexte
		if($config['context'])
			$qb->andWhere('file.context LIKE :context')->setParameter('context', $config['context']);

		// Filtrer par user
		if($config['user'] instanceof User)
			$qb->andWhere('file.createdBy = :user')->setParameter('user', $config['user']);

		// Mot clé
		if($config['search']){
			$searchTerm = trim($config['search']);
			$qb->andWhere('file.originalFileName LIKE :search OR file.title LIKE :search')
				->setParameter('search', '%'.$searchTerm.'%')
			;
		}

		return $qb->getQuery()->getResult();
	}

	/**
	 * @param File $file
	 * @throws \ImagickException
	 */
	public function rotate(File $file)
	{
		if(!in_array($file->getFiletype(), ['jpeg','jpg','png','gif']))
			return;

		// Original
		$path = $this->getFullFilePath($file);
		if(!is_file($path))
			return;

		$imagick = new \Imagick(
			$this->getFullFilePath($file)
		);
		$imagick->rotateimage('#000000', 90);
		$imagick->writeImage($path);

		// Miniatures
		foreach($this->imageVersions as $versionSlug => $config){

			$path = $this->getFullFilePath($file, $versionSlug);
			if(!is_file($path))
				continue;

			$imagick = new \Imagick(
				$this->getFullFilePath($file)
			);
			$imagick->rotateimage('#000000', 90);
			$imagick->writeImage($path);
		}

		// Permet un anticache basé sur la date d'éditoni
		$file->setUpdateDate(new \DateTime());
		$this->em->persist($file);
		$this->em->flush();
	}
}
