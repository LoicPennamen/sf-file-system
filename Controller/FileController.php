<?php

namespace LoicPennamen\FileSystemBundle\Controller;

use LoicPennamen\FileSystemBundle\Entity\File;
use LoicPennamen\FileSystemBundle\Form\FileType;
use LoicPennamen\FileSystemBundle\Repository\FileRepository;
use LoicPennamen\FileSystemBundle\Services\FileService;
use LoicPennamen\SharedRepository\DatatableService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;


/**
 * @Route("/file-system", name="filesystem_")
 */
class FileController extends AbstractController
{

    /**
     * @Route("/", name="root")
     */
    public function root()
    {
        return new Response("The file system works!", 200, [
			'X-Robots-Tag' => 'noindex'
		]);
    }

    /**
     * @Route("/get-file/{version?md}/{fileName}", name="get_file")
     */
    public function getFile(Request $request, $fileName, $version, EntityManagerInterface $em, FileService $fileService, Security $security)
    {
        $file = $em->getRepository(File::class)->findOneBy([
        	'fileName' => $fileName
		]);
        if(!$file)
        	throw new NotFoundHttpException("File not found");

        $path = $fileService->getFullFilePath($file, $version);

        // Marquer comme lu selon le contexte
		if($this->getUser() && $request->get('markAsRead'))
			$fileService->markAsRead($file, $this->getUser());

		return new BinaryFileResponse($path);
    }

	/**
	 * @Route("/search", name="search")
	 */
	public function search(FileService $fileService, DatatableService $datatableService)
	{
		$this->denyAccessUnlessGranted('searchFiles');

		$columns = $fileService->getColumns();

		return $this->render('file/search.html.twig', [
			'columns' => $columns,
			'datatableService' => $datatableService,
		]);
	}

	/**
	 * @Route("/user-files/{context}", name="search_owned")
	 */
	public function searchOwned($context, FileService $fileService, DatatableService $datatableService)
	{
		$this->denyAccessUnlessGranted('searchOwnFiles');

		$columns = $fileService->getColumns();

		return $this->render('file/search-own.html.twig', [
			'columns' => $columns,
			'datatableService' => $datatableService,
			'context' => $context,
		]);
	}

	/**
	 * @Route("/api/search", name="search_api")
	 */
	public function searchApi(Request $request, EntityManagerInterface $em, FileService $fileService, DatatableService $datatableService)
	{
		$this->denyAccessUnlessGranted('searchFiles');

		// Vars
		$repo = $em->getRepository(File::class);

		// Search
		$tableColumns = $fileService->getColumns();
		$options = $datatableService->getOptionsFromRequest($request, $tableColumns);
		$entities = $repo->search($options);

		// Formate data
		$rows = [];
		foreach ($entities as $entity)
			$rows[] = $datatableService->getRow($entity, $tableColumns);

		// Result container
		$results = [
			'data' => $rows,
			'recordsTotal' => $repo->countSearchTotal($options),
			'recordsFiltered' => $repo->countSearch($options),
		];

		return $this->json( $results );
	}

	/**
	 * @Route("/api/search-user-files", name="search_owned_api")
	 */
	public function searchOwnedApi(Request $request, EntityManagerInterface $em, FileService $fileService, DatatableService $datatableService)
	{
		$this->denyAccessUnlessGranted('searchOwnFiles');

		// Vars
		$repo = $em->getRepository(File::class);

		// Search
		$tableColumns = $fileService->getColumns();
		$options = $datatableService->getOptionsFromRequest($request, $tableColumns);

		$options['user'] = $this->getUser();
		$options['context'] = $request->get('context');

		$entities = $repo->searchForUserAndContext($options);

		// Formate data
		$rows = [];
		foreach ($entities as $entity)
			$rows[] = $datatableService->getRow($entity, $tableColumns);

		// Result container
		$results = [
			'data' => $rows,
			'recordsTotal' => $repo->countSearchTotalForUserAndContext($options),
			'recordsFiltered' => $repo->countSearchForUserAndContext($options),
		];

		return $this->json( $results );
	}

	/**
	 * @Route("/api/files-selection", name="search_and_select_api")
	 */
	public function searchForSelection(Request $request, FileService $fileService, Environment $twig)
	{
		$options = array_merge($request->request->all(), [
			'user' => $this->getUser()
		]);
		$files = $fileService->find($options);

		$html = $twig->render('file/file-selection.html.twig', [
			'files' => $files,
			'mode' => $request->request->get('mode')
		]);

		return $this->json([
			'html' => $html
		]);
	}

	/**
	 * @Route("/api/upload", name="upload")
	 */
	public function upload(Request $request, FileService $fileService)
	{
		$this->denyAccessUnlessGranted('createFile');

		try {
			$results = $fileService->handleUpload($request);
		} catch (\Exception $exception) {
			$results = [
				'error' => $exception->getMessage()
			];
		}

		return $this->json( $results );
	}

	/**
	 * @Route("/file/{id}/update", name="update")
	 */
	public function update(File $file, Request $request, TranslatorInterface $tl, EntityManagerInterface $em)
	{
		$this->denyAccessUnlessGranted("update", $file);

		// Vars
		$form = $this->createForm(FileType::class, $file);

		$form->handleRequest($request);
		if ($form->isSubmitted()) {
			if(true !== $form->isValid())
				$this->addFlash('error', $tl->trans('form_error'));

			else {
				$em->persist($file);
				$em->flush();
				$this->addFlash('success', $tl->trans('file.update_success'));
			}
		}

		return $this->render('file/update.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/file/{id}/delete", name="delete")
	 */
	public function delete(File $file, Request $request, TranslatorInterface $tl, FileService $fileService)
	{
		$this->denyAccessUnlessGranted("delete", $file);

		// Vars
		if ('POST' === $request->getMethod() && $request->get('confirm')) {
			try {
				$fileService->delete($file);
				$this->addFlash('success', $tl->trans('file.delete_success'));
				return $this->redirectToRoute('filesystem_search');

			} catch (\Exception $exception) {
				$this->addFlash('error', $exception->getMessage());
			}
		}

		return $this->render('file/delete.html.twig', [
			'file' => $file
		]);
	}

	/**
	 * @Route("/api/file/{id}/delete", name="delete_ajax")
	 */
	public function deleteApi(File $file, FileService $fileService)
	{
		$this->denyAccessUnlessGranted('delete', $file);

		$fileService->delete($file);

		return $this->json([
			'error' => null,
			'success' => true,
		]);
	}

}
