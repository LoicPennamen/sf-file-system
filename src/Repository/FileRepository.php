<?php

namespace LoicPennamen\FileSystemBundle\Repository;

use LoicPennamen\FileSystemBundle\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use LoicPennamen\SharedRepository\SharedSearchRepository;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends SharedSearchRepository
{
    public function __construct(ManagerRegistry $registry)
    {
		$this->setEntityAlias('file');
		$this->addSearchField('file.fileName');
		$this->addSearchField('file.originalFileName');

		parent::__construct($registry, File::class);
    }

    public function searchForUserAndContext(array $options)
	{
		$options['query_type'] = 'search';
		$qb = parent::getBigQueryBuilder($options);

		$this->addUserAndContext($qb, $options);

		return $qb->getQuery()->getResult();
	}

	public function countSearchTotalForUserAndContext(array $options = null)
	{
		$qb = $this->createQueryBuilder('file')
			->select("COUNT(DISTINCT(file.id))")
		;

		// $qb = $this->addMandatoryParameters($qb, $options);
		$this->addUserAndContext($qb, $options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	public function countSearchForUserAndContext(array $options)
	{
		$options['query_type'] = 'count';
		$qb = $this->getBigQueryBuilder($options);
		$this->addUserAndContext($qb, $options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	private function addUserAndContext($qb, array $options)
	{
		$qb->andWhere('file.createdBy = :user')->setParameter('user', $options['user']);
		$qb->andWhere('file.context = :context')->setParameter('context', $options['context']);
	}
}
