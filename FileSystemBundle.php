<?php

namespace LoicPennamen\FileSystemBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileSystemBundle extends Bundle
{
	public function getPath(): string
	{
		return \dirname(__DIR__);
	}
}
